import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtamponPage } from './addtampon.page';

describe('AddtamponPage', () => {
  let component: AddtamponPage;
  let fixture: ComponentFixture<AddtamponPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtamponPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtamponPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
