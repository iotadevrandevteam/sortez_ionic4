import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-parabp',
  templateUrl: './parabp.page.html',
  styleUrls: ['./parabp.page.scss'],
})
export class ParabpPage implements OnInit {
  msg:any;
  idcom:any;
  data;
  server = 'https://www.randawilly.ovh/';
  comdata: any;
  constructor(
      public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataCom: DataService,
  ) { 
      this.comdata = this.dataCom.getDataCommercant() //infocom
  		this.comdata = JSON.parse(this.comdata);
  		this.idcom = this.comdata.json.IdCommercant;
  }

  async ngOnInit() {
    let loading = await this.loadingCtrl.create({
      message: 'Traitement...'
    });
  
    loading.present();
    let url = this.server+'sortez_pro/sortez_pro_mobile/get_list_bonplan_new/';
    let body = { idcom: this.idcom };
    // alert(JSON.stringify(body));
    const headers = {"Content-Type":"application/json"}
      this.http.post(url, body, headers)
        .then(data => {
            this.data = JSON.parse(data.data);
            console.log("data"); 
            console.log(this.data);
            console.log(data); 
            alert(JSON.stringify(this.data))
            loading.dismiss();  
            // alert(JSON.stringify(this.data));
        }, error => {
          const toast = this.toastCtrl.create({
            showCloseButton: true,
            closeButtonText: 'ok',
            message: "Pas de bonplan",
            position: 'middle'
          }).then((toast)=>{
            toast.present();
          });
          loading.dismiss();
         
        });
  }
  relance(){
    let url = this.server+'sortez_pro/sortez_pro_mobile/get_list_bonplan_new/';
    let body = { idcom: this.idcom };
    const headers = {"Content-Type":"application/json"};
      this.http.post(url, body, { headers: headers })
        .then(data => {
            this.data=data;
            console.log(this.data);
        }, error => {
          let toast = this.toastCtrl.create({
            showCloseButton: true,
            closeButtonText: 'ok',
            message: "Pas de bonplan",
            position: 'middle'
          }).then((toast)=>{
            toast.present();
          })
        });
  }
  async delete(id){
    let loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    loading.present();
    let url = this.server+'sortez_pro/sortez_pro_mobile/delete_bonplan_by_id/';
    let body = JSON.stringify({ id: id });
    const headers = {"Content-Type":"application/json"};
      this.http.post(url, body, { headers: headers })
        .then(data => {
            loading.dismiss();
            alert('bonplan supprimé');
            this.ngOnInit();
        }, error => {
          loading.dismiss();
          alert('Erreur de suppression');
        });
  }
  parabon() {
    let IdCommercant = this.idcom;
    // this.navCtrl.push(ParabonPage, { IdCommercant });
    this.navCtrl.navigateForward('parabon');
  }
  set_photo(id){
    // this.fileChooser.open()
    // .then(uri =>  this.upload(uri,id))
    // .catch(e => console.log(e));
  }
  async upload(uri,id) {
    let loading = await this.loadingCtrl.create({
      message: 'Chargement...'
    });
    loading.present();
    let lettre_aleatoire='photo_'+id;
  //   let options: FileUploadOptions = {
  //     fileKey: 'file',
  //     fileName: lettre_aleatoire+'-'+this.idcom+'-p.jpg',
  //     headers: {}
  //  }
    // this.fileTransfer.upload(uri, this.server+'sortez_pro/sortez_pro_mobile/save_bonplan_image/', options).then((data) => {
      //alert(JSON.stringify(data));
      let url=this.server+'sortez_pro/sortez_pro_mobile/save_bonplan_data_image/';
      let body = JSON.stringify({id_bonplan:id,idcom:this.idcom,nom_photo:lettre_aleatoire+'-'+this.idcom+'-p.jpg'});
      console.log(body);
      // alert(body);
      const headers = {"Content-Type":"application/json"}
      this.http.post(url, body, headers).then(data => {
              console.log(data);
              loading.dismiss();
              alert('Image importé');
              this.relance();
            }, error => {
              loading.dismiss();
              alert('erreure d\'enregistrement');
            });
    // }, (err) => {
    //   alert("Une erreur c'est produit pendant l'importation, veuillez réessayer plus tard!");
    //   console.log('Erreur');
    // })
 }
 async delete_photo(id){
  let loading = await this.loadingCtrl.create({
    message: 'Chargement...'
  });
  loading.present();
  let url=this.server+'sortez_pro/sortez_pro_mobile/delete_bonplan_data_image/';
  let body = JSON.stringify({id_bonplan:id,idcom:this.idcom});
  // alert(this.idcom);
  console.log(body);
  const headers = {"Content-Type":"application/json"}
    this.http.post(url, body, headers).then(data => {
          console.log(data);
          loading.dismiss();
          alert('Image importé');
        }, error => {
          loading.dismiss();
          // alert('erreure d\'enregistrement');
          alert('Image supprimé');
          this.relance();
        });
 }
 update(id){
  let url = this.server+'sortez_pro/sortez_pro_mobile/get_list_bonplan_by_id_new/';
  let body = {id:id, idcom: this.idcom };
  const headers = {"Content-Type":"application/json"}
    this.http.post(url, body, headers).then(data => {
          console.log(this.data);
          this.dataCom.setData(data.data);
          alert(JSON.stringify(data.data));
          this.navCtrl.navigateForward('modifbonplan');
          // this.navCtrl.push(ModifbonplanPage,{data});
      }, error => {
        let toast = this.toastCtrl.create({
          showCloseButton: true,
          closeButtonText: 'ok',
          message: "Pas de bonplan",
          position: 'middle'
        }).then((toast)=>{
          toast.present();
        });
      });
 }

}
