import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParabpPage } from './parabp.page';

describe('ParabpPage', () => {
  let component: ParabpPage;
  let fixture: ComponentFixture<ParabpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParabpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParabpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
