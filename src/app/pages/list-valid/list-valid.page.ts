import { Component, OnInit } from '@angular/core';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-list-valid',
  templateUrl: './list-valid.page.html',
  styleUrls: ['./list-valid.page.scss'],
})
export class ListValidPage implements OnInit {
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  carte;
  server = 'https://www.randawilly.ovh/';
  infocom: any;
	idcom: any;
  comdata: any;
  results;

  constructor(public navCtrl: NavController,private toastCtrl: ToastController,public http: HTTP,public storageService: StorageService,
    public dataCom: DataService,private barcodeScanner: BarcodeScanner) {
    this.comdata = this.dataCom.getDataCommercant();
  		this.comdata = JSON.parse(this.comdata);
  		this.idcom = this.comdata.json.IdCommercant;
      this.infocom = Array.of(this.comdata);
      // alert(JSON.stringify(this.infocom));
    this.barcodeScannerOptions = {
      prompt:"Pointer votre caméra vers un Code barre",
      torchOn:false,
      formats : "QR_CODE",
    };
   }

  ngOnInit() {
  }

  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        // alert("Barcode data " + JSON.stringify(barcodeData));
        // this.scannedData = barcodeData;
        if(barcodeData != null){
          this.redirectremise(barcodeData);
        }
        else{
          alert('une erreur s\'est produite');
        }
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  redirectremise(barcodeData){
        this.carte = barcodeData["text"];
        let login = this.infocom[0].json.Login;
        let url=this.server+'sortez_pro/sortez_pro_mobile/check_scan_card_new/';
        let body = {num_card_scan:this.carte,login:login};
        console.log(body);
        var headers = { "Content-Type": 'application/json' };
        this.http.post(url,body, headers).then(data => {
          console.log((data)); 
          // alert(JSON.stringify(data))
          console.log(data);
            this.results=data;
            let results = JSON.parse(this.results.data);
            const toast = this.toastCtrl.create({
              showCloseButton:false,
              message: JSON.stringify(results),
              position: 'top',
              duration: 3000
            }).then((toastData)=>{
              toastData.present();
            });
                  let url= this.server+'sortez_pro/sortez_pro_mobile/appremise2/';
                  let body = {num_card_scan:this.carte,login:login};
                  console.log(body);
                  // alert(url);
                  // alert(JSON.stringify(body));
                  var headers = { "Content-Type": 'application/json' };
                    this.http.post(url,body, headers).then(data => {
                      console.log((data)); 
                      // alert(JSON.stringify(data))
                      if(data !=null){
                        console.log(data);
                        // alert('lasa zao');
                        this.dataCom.setvalid(data);
                        this.navCtrl.navigateForward('remise')
                      }else{
                        alert('Ce client ne dispose pas encore d\'une carte Privilege !');
                        const toast = this.toastCtrl.create({
                          showCloseButton:false,
                          message: "Ce client ne dispose pas encore d'une carte Privilege !",
                          position: 'top'
                          }).then((toastData)=>{
                            toastData.present();
                          });
                        }
                    }, error => {
                      alert('error');
                      console.log(error);
                    });
        }, error => {
          alert(error);
          console.log(error);
        });
  }
}
