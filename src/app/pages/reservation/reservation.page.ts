import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})
export class ReservationPage implements OnInit {
  server = 'https://www.randawilly.ovh/';
	mail: any;
	inforeserv;
	idcom: any;
	resdata: any;
  session;
  client;
  constructor(
    public http: HTTP,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public storageService: StorageService,
    public datareserv: DataService,) 
    { 
      this.resdata = this.datareserv.getListReservation();
      this.resdata = JSON.parse(this.resdata);
      this.inforeserv = this.resdata.infoclient;
      alert(JSON.stringify(this.inforeserv))
      // alert(JSON.stringify(this.inforeserv[0].infoclient));

    }

  ngOnInit() {
  }
  validatebonplan(bonplan_commercant_id,IdUser,id){
    let url=this.server+'sortez_pro/sortez_pro_mobile/fidelity_card_operations/';
    let body = {bonplan_commercant_id:bonplan_commercant_id,IdUser:IdUser,id:id};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
    this.http.post(url, body, headers).then(data => {
        console.log(data);
        if(data==null){
          let data=null;
          // this.navCtrl.push(BonplanvalidPage,{data});
        }
        // this.navCtrl.push(BonplanvalidPage,{data});
      }, error => {
        console.log("erreur");
        alert("Ce client ne dispose pas encore d'une carte Privilege !");
    });
  }
  client_details(idclientdcom,Login){
    let login=Login;
    let url=this.server+'sortez_pro/sortez_pro_mobile/get_client_detail_new/';
    let body = {id_client:idclientdcom};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, headers).then(data => {
            console.log(data);
            this.client = data.data;
            // this.navCtrl.push(ClientdetailPage,{data,login});
            this.datareserv.setDataclient(this.client, login);
            this.navCtrl.navigateForward('clientdetail');
          }, error => {
             console.log("erreur");
             alert("Une erreur s'est produite!");
          });
      }
      sendmsg(num,nom,ville,numcom){   
        // alert(num +';'+nom+';'+ville +';'+numcom);      
        // this.navCtrl.push(SendsmsPage,{num,nom,ville,numcom});
      }

}
