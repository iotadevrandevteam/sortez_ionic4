import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-clientdetail',
  templateUrl: './clientdetail.page.html',
  styleUrls: ['./clientdetail.page.scss'],
})
export class ClientdetailPage implements OnInit {
  server = 'https://www.randawilly.ovh/';
	mail: any;
	inforeserv: any;
	idcom: any;
	resdata: any;
  session;
  client;
  infoclient: any;
  login;
  infoconso;
  constructor(
    public http: HTTP,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public storageService: StorageService,
    public datareserv: DataService,
  ) { 
    this.resdata = this.datareserv.getData();
    this.resdata = JSON.parse(this.resdata);
    this.infoclient = Array.of(this.resdata);
    alert(JSON.stringify(this.infoclient));
    // this.login = this.infoclient[0].login;
  }

  ngOnInit() {
  }
  infoconsoPage(iduser){
    let login=this.login;
    //alert(login);
    let url=this.server+'sortez_pro/sortez_pro_mobile/get_infoconso_new/';
    let body = {id_client:iduser,login:login};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, headers).then(data => {
            console.log(data);
            // this.navCtrl.push(InfoconsoPage,{data});
            this.datareserv.setData(data.data);
            this.navCtrl.navigateForward('infoconso');
          }, error => {
             console.log("erreur");
             //alert("Une erreur s'est produite!");
          });
  }

}
