import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-parametre',
  templateUrl: './parametre.page.html',
  styleUrls: ['./parametre.page.scss'],
})
export class ParametrePage implements OnInit {
  res:any;
  msg:any;
  datacomData:any;
  idcom;
  server = 'https://www.randawilly.ovh/';
  constructor(
      public http: HTTP,
      public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
      public dataserv: DataService,
      public alertCtrl:AlertController,
  ) { 
      this.datacomData = this.dataserv.getDataCommercant(); //infocom
      this.datacomData = JSON.parse(this.datacomData);
      this.idcom = this.datacomData.json.IdCommercant;
      this.msg = Array.of(this.datacomData);
  }

  ngOnInit() {
  }
  ParabonPages(idcom){
    let msg=idcom;
    let url=this.server+'sortez_pro/sortez_pro_mobile/getbonplan_new/';
    let body = {IdCommercant:msg};
    const headers ={"Content-Type":"application/json"};
        this.http.post(url, body, {headers: headers})
        .then(data1 => {
          if(data1==null){
            // this.navCtrl.push(ParabonPage,{idcom});
          }
          else{
            
            let msg=idcom;
            let url=this.server+'sortez_pro/sortez_pro_mobile/getbonplannbre_new/';
            let body = {IdCommercant:msg};
            const headers = {"Content-Type":"application/json"};
                this.http.post(url, body, {headers: headers})
                .then(data => {
                  if(data==null){
                    // this.navCtrl.push(ParabonPage,{idcom});
                  }
                  else{
                    // this.navCtrl.push(BonplanlistPage,{data1,msg,data});
                  }
                  console.log(data);
                    
                  }, error => {
                     console.log("erreur")
                  });
          }
            
          }, error => {
             console.log("erreur")
          });

}
rd(idcom){
  let msg=idcom;
  let datas=this.msg;
  let url=this.server+'sortez_pro/sortez_pro_mobile/getremisedirect_new/';
  let body = {IdCommercant:msg};
  const headers = {"Content-Type":"application/json"};
  
      this.http.post(url, body, headers)
      .then(data => {
        console.log(data);
        if(data==null){
          // this.navCtrl.push(AddremisePage,{datas});
          this.dataserv.setData(datas);
          this.navCtrl.navigateForward('addremise');
        }
        else{
          // this.navCtrl.push(ParardPage,{data});
          this.dataserv.setData(data.data);
            this.navCtrl.navigateForward('parard');
        }
        }, error => {
           console.log("erreur")
        });
}
cp(idcom){
  let msg=idcom;
  let datas=this.msg;
  let url=this.server+'sortez_pro/sortez_pro_mobile/getcapital_new/';
  let body = {IdCommercant:msg};
  const headers = {"Content-Type":"application/json"};
  
      this.http.post(url, body, headers)
      .then(data => {
        console.log(data);
        if(data==null){
          // this.navCtrl.push(AddcapitalPage,{datas});
          this.dataserv.setData(datas);
          this.navCtrl.navigateForward('addcapital');
        }
        else{
          // this.navCtrl.push(ParaccPage,{data});
          this.dataserv.setData(data.data);
            this.navCtrl.navigateForward('paracc');
        }
        }, error => {
           console.log("erreur")
        });
}
ct(idcom){
  let msg=idcom;
  let datas=this.msg;
  let url=this.server+'sortez_pro/sortez_pro_mobile/getcoupdetampon_new/';
  let body = {IdCommercant:msg};
  const headers = {"Content-Type":"application/json"};
  return new Promise(resolve => {
      this.http.post(url, body,headers).then(data => {
          console.log(data);
          if(data==null){
            // this.navCtrl.push(AddtamponPage,{datas});
            this.dataserv.setData(datas);
            this.navCtrl.navigateForward('addtampon');
          }
          else{
            // this.navCtrl.push(ParactPage,{data});
            this.dataserv.setData(data.data);
            this.navCtrl.navigateForward('paract');
          }
          
        }, error => {
           console.log("erreur")
        });
      });

}
}
