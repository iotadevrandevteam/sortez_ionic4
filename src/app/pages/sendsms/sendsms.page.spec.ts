import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendsmsPage } from './sendsms.page';

describe('SendsmsPage', () => {
  let component: SendsmsPage;
  let fixture: ComponentFixture<SendsmsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendsmsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendsmsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
