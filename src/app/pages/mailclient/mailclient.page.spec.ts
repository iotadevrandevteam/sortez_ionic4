import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailclientPage } from './mailclient.page';

describe('MailclientPage', () => {
  let component: MailclientPage;
  let fixture: ComponentFixture<MailclientPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailclientPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailclientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
