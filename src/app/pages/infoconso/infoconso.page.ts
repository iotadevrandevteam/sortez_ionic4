import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-infoconso',
  templateUrl: './infoconso.page.html',
  styleUrls: ['./infoconso.page.scss'],
})
export class InfoconsoPage implements OnInit {
  server = 'https://www.randawilly.ovh/';
	mail: any;
	inforeserv: any;
	idcom: any;
	resdata: any;
  session;
  client;
  infoclient: any;
  login;
  infoconso: any;
  resdata2 :any;
  data : any;
  widthright;
  widthleft;
  resplat;
  constructor(
    public http: HTTP,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public storageService: StorageService,
    public datareserv: DataService,
  ) { 
    this.resdata = this.datareserv.getData();
    this.resdata = JSON.parse(this.resdata);
    this.infoconso = Array.of(this.resdata);
    alert(JSON.stringify(this.infoconso));
    for(let res1  of this.infoconso){
      //console.log(res1.fidelity.offre);
      if(res1.fidelity !=false){

        if(res1.offre =='capital'){
          var obj=res1.fidelity.objectif;
          var cumul=res1.fidelity.solde_capital;
          this.widthleft=(parseInt(cumul)*100)/(parseInt(obj));
          this.widthright=(100-(this.widthleft *1));
        }
        if(res1.offre =='tampon'){
          var obj2=res1.fidelity.tampon_value;
          var cumul2=res1.fidelity.solde_tampon;
          this.widthleft=(parseInt(cumul2)*100)/(parseInt(obj2));
          this.widthright=(100-(this.widthleft *1));
        }
        
      }
    

      for(let infoplat  of res1.plat){
        this.resplat+=(infoplat.nbre_platDuJour*1);
      }
    }
  }

  ngOnInit() {
  }
  async reinitialisation(fiche_id,type_offre,id_user,id_commercant,montant_init){
    let loading = await this.loadingCtrl.create({
      message: 'Traitement...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 2000)
    let url=this.server+'sortez_pro/sortez_pro_mobile/reinistialisation/';
    let body = {fiche_id:fiche_id,type_offre:type_offre,id_user:id_user,id_commercant:id_commercant,montant_init:montant_init};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, {headers: headers})
        .then(data => {
            console.log(data);
            if(data.data =='ok'){
              let toast = this.toastCtrl.create({
                message: 'Offre remis à zéro !',
                duration: 3000,
                position: 'top'
              }).then((toast)=>{
                toast.present();
              })
              this.navCtrl.pop();
              loading.dismiss();
            }else{
              let toast = this.toastCtrl.create({
                message: 'Une erreur s\'est produite !',
                duration: 3000,
                position: 'top'
              }).then((toast)=>{
                toast.present();
              })
              loading.dismiss();
            }
          }, error => {
             console.log("erreur");
             alert("Une erreur s'est produite!");
             loading.dismiss();
          });
      }

      reinitialisation_plat(){
        let toast = this.toastCtrl.create({
          message: 'Pas encore disponible !',
          duration: 3000,
          position: 'middle'
        }).then((toast)=>{
          toast.present();
        })
      }
      detail_bonplan(iduser){
        // this.navCtrl.push(DetailBonplanPage)
        let url='https://www.sortez.org/sortez_pro/sortez_pro_mobile/details_bonplan/';
        let body = JSON.stringify({id:iduser});
        console.log(body);
        const headers = {"Content-Type":"application/json"};
            this.http.post(url, body, headers)
            .then(data => {
                console.log(data);
                let toast = this.toastCtrl.create({
                  message: 'Succès',
                  duration: 3000,
                  position: 'top'
                }).then((toast)=>{
                  toast.present();
                })
              
        
              
               
                // this.navCtrl.push(DetailBonplanPage,{data})
              }, error => {
                alert("Champ incorrecte ou manquant")
              });
      }
      client_details(idclientdcom){
        // let login=this.id;
        let url=this.server+'sortez_pro/sortez_pro_mobile/get_client_detail/';
        let body = {id_client:idclientdcom};
        console.log(body);
        const headers ={"Content-Type":"application/json"};
       this.http.post(url, body, headers).then(data => {
                console.log(data);
                // this.navCtrl.push(ClientdetailPage,{data,login});
              }, error => {
                 console.log("erreur");
                 alert("Une erreur s'est produite!");
              });
          }
          sendmsg(num,nom,ville,numcom){   
            //  alert(num); 
            //  alert(nom);   
            //  alert(numcom) ;
            //  alert(ville);
            // this.navCtrl.push(SendsmsPage,{num,nom,ville,numcom});
          }

          sendsmsOne(num){
          //   this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS);
          //   this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE);
          //  var tel= num.replace(" ", '');
          //  this.sms.send(tel, 'votre message');
          }

}
