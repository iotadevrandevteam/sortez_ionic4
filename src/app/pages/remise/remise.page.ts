import { Component, OnInit, Directive, Input, ViewChild } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';
import { HTTP } from '@ionic-native/http/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-remise',
  templateUrl: './remise.page.html',
  styleUrls: ['./remise.page.scss'],
})
export class RemisePage implements OnInit {
    @ViewChild('myInput', {static: true}) myInput;
    server = 'https://www.randawilly.ovh/';
    msg:any;
    msg2:any;
    get: any;
    solde_capital;
    solde_remise;
  
  constructor(public navCtrl: NavController,private toastCtrl: ToastController,public http: HTTP,public storageService: StorageService,
    public dataCom: DataService,private keyboard : Keyboard, private loadingCtrl : LoadingController ) { 
      this.get = this.dataCom.getvalid();
      this.msg = JSON.parse(this.get.data)
      console.log(this.msg);
      this.msg = Array.of(this.msg);
      // alert(JSON.stringify(this.msg));
      this.msg2=this.msg[0].json.assoc_client;
      if(this.msg[0].json.oFicheclient === null){
        this.solde_capital=0;
      }
      if(this.msg[0].json.assoc_client_remise === null){
        this.solde_remise = 0;
      }

  }

  ngOnInit() {
    this.keyboard.show();
  }
  widthleft(cumule,obj){
    if(cumule === null){
      cumule=0;
    }
    console.log(cumule);
    if(obj != null){
      var res=((parseInt(cumule)*100)/parseInt(obj));
     return (res+'%');
    }else{
      return ('100%');
    }
  }
  widthright(cumule,obj){
    if(cumule === null){
      cumule=0;
    }
    console.log(cumule);
    if(obj != null){
      var res_left=((parseInt(cumule)*100)/parseInt(obj));
      var res=100-(res_left);
     return (res+'%');
    }else{
   return ('0%');
    }
  }
  add_card_tampon_mobile(unite_tampon_value,id_card,id_commercant,id_user){
    let url=this.server+'sortez_pro/sortez_pro_mobile/add_card_tampon_mobile_new/';
    // alert(url);
    let body = {unite_tampon_value:unite_tampon_value,id_card:id_card,id_commercant:id_commercant,id_user:id_user};
    console.log(body);
    // alert(JSON.stringify(body));
    const headers = {"Content-Type": "application/json"};
    this.http.post(url, body, headers).then(data => {
            console.log(data);
            // alert(JSON.stringify(data));
            let toast = this.toastCtrl.create({
              message: 'Opération validé avec succès',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            });
            this.navCtrl.pop();
          }, error => {
             console.log("erreur")
             let toast = this.toastCtrl.create({
              message: 'Erreur inconnue',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            });
            
          });
  }
  add_card_capital_mobile(unite_capital_value,id_card,id_commercant,id_user){
    let url=this.server+'sortez_pro/sortez_pro_mobile/add_card_capital_mobile_new/';
    let body = {unite_capital_value:unite_capital_value,id_card:id_card,id_commercant:id_commercant,id_user:id_user};
    console.log(body);
    // alert(JSON.stringify(body));
    const headers = {"Content-Type": "application/json"};
    this.http.post(url, body, {headers: headers}).then(data => {
            console.log(data);
            // alert(JSON.stringify(data));
            let toast = this.toastCtrl.create({
              message: 'Opération validé avec succès',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            });
            this.navCtrl.pop();
          }, error => {
             console.log("erreur")
             let toast = this.toastCtrl.create({
              message: 'Erreur inconnue',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            });
          });
  }
  add_card_remise_mobile(unite_remise_value,id_card,id_commercant,id_user){
    let url= this.server+'sortez_pro/sortez_pro_mobile/add_card_remise_mobile_new/';
    let body = {unite_remise_value:unite_remise_value,id_card:id_card,id_commercant:id_commercant,id_user:id_user};
    console.log(body);
    // alert(JSON.stringify(body));
    const headers = {"Content-Type": 'application/json'};
    this.http.post(url, body, headers).then(data => {
            console.log(data);
            // alert(JSON.stringify(data));
            const toast = this.toastCtrl.create({
              message: 'Opération validé avec succès',
              duration: 3000,
              position: 'top'
            }).then((toastData)=>{
              toastData.present();
            });
            this.navCtrl.pop();
          }, error => {
             console.log("erreur")
             let toast = this.toastCtrl.create({
              message: 'Erreur inconnue',
              duration: 3000,
              position: 'top'
            });
          });
  }

}
