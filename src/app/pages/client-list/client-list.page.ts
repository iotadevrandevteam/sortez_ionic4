import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  styleUrls: ['./client-list.page.scss'],
})
export class ClientListPage implements OnInit {
  res:any;
  msg:any;
  clientData:any;
  comData: any;
  idcom;
  server = 'https://www.randawilly.ovh/';
  selectedArray :any = [];
  selectedArrayMail :any = [];
  categorysms;
  categorymail;
  result:any;
  datas:any;
  id:any;
  imageURI:any;
  imageFileName:any;
  liste_global;
  changed_val:number=0;
  private imageSrc: string;
  private images: any[]= [];
  public form = [];
  
  constructor(
      private file: File,
      public http: HTTP,
      public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
      public dataserv: DataService,
      public alertCtrl:AlertController,
      private diagnostic: Diagnostic,
      private imagePicker: ImagePicker,
      private transfer: FileTransfer,
      private fileChooser: FileChooser,
  ) {
         this.clientData = this.dataserv.getListClient();
        //  alert(JSON.stringify(this.clientData));
          this.msg = Array.of(this.clientData);
          this.comData= this.dataserv.getDataCommercant();
          this.comData = JSON.parse(this.comData);
          this.idcom = this.comData.json.IdCommercant;
          for (let i = 1;i < 5;i++){
            this.images.push({
              url: 'assets/${i}.png'
            });
          }
          this.images[0].title='Ajouter une image pour votre logo';
          this.images[2].title='Selectionner votre image';
  }
  fileTransfer: FileTransferObject = this.transfer.create();

  ngOnInit() {
  }
  viewDetailsClient(id_client,id_commercant){

    let url=this.server+'sortez_pro/sortez_pro_mobile/showsolde_new/';
    let body = {id_client:id_client,id_commercant:id_commercant};
    const headers = {"Content-Type":"application/json"};
    this.http.post(url, body, headers).then(data => {
            // this.navCtrl.push(ListDetailPage,{data});
          }, error => {
             console.log("erreur")
          });
  }
  sendMail(array){
    if (typeof array !== 'undefined' && array.length > 0) {
      let ids=this.id;
      let msg=this.msg;
      // this.navCtrl.push(MailclientPage,{ids,msg,array});
    }else{
      let toast = this.toastCtrl.create({
        message: 'Veuillez choisir un ou plusieurs clients',
        duration: 3000,
        position: 'middle'
      }).then((toast)=>{
        toast.present();
      });
    }
  }
  choosefile(){
    this.fileChooser.open()
    .then(uri =>  this.upload(uri))
    .catch(e => console.log(e));
    // TODO rename open to pick, select, or choose.
  }
  async upload(uri) {
    let loader = await this.loadingCtrl.create({
      message: "Uploading..."
    });
    loader.present();
    setTimeout(() => {
      loader.dismiss();
    }, 3000);
    // const fileTransfer: FileTransferObject = this.transfer.create();
    let alphabet = ['','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
// nombre aléatoire entre 1 et 25
let random = Math.ceil(Math.random() * 25);
// on recupere une entrée aléatoire du tableau
var lettre_aleatoire = alphabet[random];

    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: lettre_aleatoire+'.jpg',
      headers: {}
   }
    this.fileTransfer.upload(uri, this.server+'front/Upload/getimage/', options)
    .then((data) => {
      alert('Importation image réussie avec succès');
      console.log('Success');
    }, (err) => {
      alert("Une erreur c'est produit pendant l'importation, veuillez réessayer plus tard!");
      console.log('Erreur');
        })
 }

  
  download() {
    const url = 'http://www.example.com/file.pdf';
    this.fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
      console.log('download complete: ' + entry.toURL());
    }, (error) => {
      // handle error
    });
  }

  client_details(idclientdcom){
    let login=this.id;
    let url=this.server+'sortez_pro/sortez_pro_mobile/get_client_detail/';
    let body = {id_client:idclientdcom};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, headers)
        .then(data => {
            console.log(data);
            // this.navCtrl.push(ClientdetailPage,{data,login});
          }, error => {
             console.log("erreur");
             alert("Une erreur s'est produite!");
          });
      }

      infoconsoPage(iduser,login){
        //alert(login);
        let url=this.server+'sortez_pro/sortez_pro_mobile/get_infoconso/';
        let body = {id_client:iduser,login:login};
        console.log(body);
        const headers = {"Content-Type":"application/json"};
            this.http.post(url, body, headers)
            .then(data => {
                console.log(data);
                // this.navCtrl.push(InfoconsoPage,{data,login});
              }, error => {
                 console.log("erreur");
                 alert("Une erreur s'est produite!");
              });
      }

      SendsmsPage(list){
        if (typeof list !== 'undefined' && list.length > 0) {
          // this.navCtrl.push(SendsmsPage, {list});  
        }else{
          let toast = this.toastCtrl.create({
            message: 'Veuillez choisir un ou plusieurs clients',
            duration: 3000,
            position: 'middle'
          }).then((toast)=>{
            toast.present();
          })
        }
        
        }
      async uploadFile() {
        let loader = await this.loadingCtrl.create({
          message: "Uploading..."
        });
        loader.present();
        setTimeout(() => {
          loader.dismiss();
        }, 1000);
        
        const fileTransfer: FileTransferObject = this.transfer.create();
      
        let options: FileUploadOptions = {
          fileKey: 'ionicfile',
          fileName: 'ionicfile',
          chunkedMode: false,
          mimeType: "image/jpeg",
          headers: {}
        }
      
        fileTransfer.upload("../../assets/add_icon.png", 'http://192.168.0.7:8080/api/uploadImage', options)
          .then((data) => {
          console.log(data+" Uploaded Successfully");
          this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
          loader.dismiss();
          this.presentToast("Image uploaded successfully");
        }, (err) => {
          console.log(err);
          loader.dismiss();
          this.presentToast(err);
        });
      }
      presentToast(msg) {
        const toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom'
        }).then((toast)=>{
          toast.present();
        })
      }
      selectMember(data){
        if (data.checked == true) {
           this.selectedArray.push(data);
         } else {
          let newArray = this.selectedArray.filter(function(el) {
            return el.testID !== data.testID;
         });
          this.selectedArray = newArray;
        }
        console.log(this.selectedArray);
       }
       addCheckbox(event, iduser) { 
        if (event.checked) {
          this.selectedArray.push(iduser);
        } else {
          let index = this.removeCheckedFromArray(iduser);
          this.selectedArray.splice(index,1);
          this.selectedArrayMail.splice(index,1);
        }
        console.log(this.selectedArray);
      }
    
      //Removes checkbox from array when you uncheck it
      removeCheckedFromArray(checkbox) {
         this.selectedArray.findIndex((category)=>{
           this.categorysms === checkbox;
        });
      }
      
      presentPrompt() {
        const alert = this.alertCtrl.create({
          header: 'Par nom',
          inputs: [
            {
              name: 'Nom',
              placeholder: 'Nom',
              type: 'text'
            }
          ],
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              handler: data => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'OK',
              handler: data => {
                if (data.Nom) {
              let filtre=[];
              if(this.changed_val == 0){
                this.liste_global=this.msg;
              }
               this.liste_global.forEach(function (value) {
                if(value.Nom.match(data.Nom)){
                  filtre.push(value);
                }
            }); 
            this.changed_val=+1;
            this.msg=filtre;
            console.log('global');
            console.log(this.liste_global);
              } else {
                  // invalid login
                  return false;
                }
              }
            }
          ]
        }).then((alert)=>{
          alert.present();
        });
}
annif(){
  if(this.changed_val == 0){
    this.liste_global=this.msg;
  }
 let nbjan=0;
 let nbfev=0;
 let nbmar=0;
 let nbavr=0;
 let nbmai=0;
 let nbjun=0;
 let nbjuil=0;
 let nbaout=0;
 let nbsept=0;
 let nboct=0;
 let nbnov=0;
 let nbdec=0;
  let clients=this.liste_global;
    clients.forEach(function (value) {
    let mois=value.DateNaissance.split('-')[1];
    if(mois == '01'){
      nbjan+=1;
    }
    if(mois == '02'){
      nbfev+=1;
    }
    if(mois == '03'){
      nbmar+=1;
    }
    if(mois == '04'){
      nbavr+=1;
    }
    if(mois == '05'){
      nbmai+=1;
    }
    if(mois == '06'){
      nbjun+=1;
    }
    if(mois == '07'){
      nbjuil+=1;
    }
    if(mois == '08'){
      nbaout+=1;
    }
    if(mois == '09'){
      nbsept+=1;
    }
    if(mois == '10'){
      nboct+=1;
    }
    if(mois == '11'){
      nbnov+=1;
    }
    if(mois == '12'){
      nbdec+=1;
    }
});

  const alert = this.alertCtrl.create({
    header: 'Par mois',
    inputs: [
      {
        name: 'mois',
        type: 'radio',
        label:'Janvier'+' ('+nbjan+')',
        value:'01'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Février'+' ('+nbfev+')',
        value:'02'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Mars'+' ('+nbmar+')',
        value:'03'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Avril'+' ('+nbavr+')',
        value:'04'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Mai'+' ('+nbmai+')',
        value:'05'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Juin'+' ('+nbjun+')',
        value:'06'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Juillet'+' ('+nbjuil+')',
        value:'07'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Août'+' ('+nbaout+')',
        value:'08'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Septembre'+' ('+nbsept+')',
        value:'09'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Octobre'+' ('+nboct+')',
        value:'10'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Novembre'+' ('+nbnov+')',
        value:'11'
      },
      {
        name: 'mois',
        type: 'radio',
        label:'Décembre'+' ('+nbdec+')',
        value:'12'
      },
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        handler: data => {
          console.log(data);
        }
      },
      {
        text: 'OK',
        handler: data => {
          if (data) {
            let filtre=[];
            if(this.changed_val == 0){
              this.liste_global=this.msg;
            }
             this.liste_global.forEach(function (value) {
               let mois=value.DateNaissance.split('-');
              if(mois[1] == data){
                filtre.push(value);
              } 
          });
          this.changed_val=+1;
          this.msg=filtre;
          console.log('global');
          console.log(filtre);
            } else {
                return false;
              }
        }
      }
    ]
  }).then((alert)=>{
    alert.present();
  });
}
showall(){
 if(this.liste_global !=undefined){
  this.msg=this.liste_global;
}
}
filter_advanced(){
  const alert = this.alertCtrl.create({
    header: 'Rubriques',
    inputs: [
      {
        name: 'bonplan',
        type: 'radio',
        label:'Bons Plans',
        value:'bp'
      },
      {
        name: 'fidelite',
        type: 'radio',
        label:'Fidelisation',
        value:'fd'
      },
      {
        name: 'plat',
        type: 'radio',
        label:'Plat du jour',
        value:'plat'
      },
      {
        name: 'sejour',
        type: 'radio',
        label:'Réservation hébergement',
        value:'sejour'
      },
      {
        name: 'table',
        type: 'radio',
        label:'Réservation restauration',
        value:'table'
      },
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        handler: data => {
          // console.log(data);
        }
      },
      {
        text: 'Voir les résultats',
        handler: data => {
          if (data) {
            let filtre=[];
            if(this.changed_val == 0){
              this.liste_global=this.msg;
            }
             this.liste_global.forEach(function (value) {
              if(data =="bp"){
                
                if(value.bonplan !=false){
                  filtre.push(value);
                }
              }else if(data =="fd"){
                if(value.solde !='undefined' && value.solde != 0 ){
                  filtre.push(value);
                }
              }else if(data == 'plat'){
                if(value.plat.length >0){
                  filtre.push(value);
                }
              }
          });
          this.changed_val=+1;
          //console.log(filtre);
          this.msg=filtre;
            } else {
                return false;
              }
        }
      }
    ]
  }).then((alert)=>{
    alert.present();
  });
}
sendMailSms(msgcl){
  let url=this.server+'sortez_pro/sortez_pro_mobile/get_login_by_id/';
        let body = {login:this.id};
        console.log(body);
        const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, headers).then(data => {
              if(this.selectedArray !=""){
                msgcl=this.selectedArray;
                console.log('ok');
                console.log(msgcl);
              }
              let msg=[];
              let infocom=[];
              msgcl.forEach(function (value) {
                let array={'mail':value.Email,'idcom':value.id_commercant,'tel':value.Portable,'logo_mobile':value.commercant.logo_mobile,'image_mobile':value.commercant.image_mobile,'id_ionauth':value.commercant.user_ionauth_id}
                msg.push(array);
                });
                infocom=msg[0];
                console.log('infocom');
                console.log(infocom);
              // this.navCtrl.push(MailclientPage,{msg,infocom,data})
              }, error => {
                 console.log("erreur");
                 alert("Une erreur s'est produite!");
              });
}
widthleft(cumule,obj){
  if(obj != null){
    var res=((parseInt(cumule)*100)/parseInt(obj));
   return (res+'%');
  }else{
    return ('100%');
  }
}
widthright(cumule,obj){
  if(obj != null){
    var res_left=((parseInt(cumule)*100)/parseInt(obj));
    var res=100-(res_left);
   return (res+'%');
  }else{
 return ('0%');
  }
  
}
// exportcsv() {
//   let data=[];
//   for(let datas of this.msg ){
//     console.log(datas)
//   var csved={"Nom_client": datas.Nom+' '+datas.Prenom,"Adresse":datas.Adresse,'CodePostal':datas.CodePostal,'DateNaissance':datas.DateNaissance,'mail_client':datas.Email,"tel_client":datas.Portable,'Ville':datas.Ville}
//    data.push(csved);
//   }
//   let csv = papa.unparse(data);
//   console.log(csv)
//   let path = this.file.externalRootDirectory;
//   // this.diagnostic.requestExternalStorageAuthorization().then(() => {
//   //   console.log(path);
//   //   this.file.createDir(path, 'reservation_sortez', false);
//   //   let path2 = path + "reservation_sortez/";
//   //   //alert(path2);
//   //   let fileName = "reservation_plat.csv";
//   //   let isAppend = true;
//   //   // this.diagnostic.requestExternalStorageAuthorization().then(() => {
//   //   //   this.file.createFile(path2, fileName, isAppend).then(
//   //   //     (files) => {
//   //   //       this.file.writeFile(path2, fileName, csv, { replace: true })
//   //   //         .then(() => {
//   //   //           let toast = this.toastCtrl.create({
//   //   //             message: 'Votre fichier se trouve dans reservation_sortez/reservation_plat.csv',
//   //   //             duration: 5000,
//   //   //             position: 'middle'
//   //   //           });
//   //   //           toast.present();
//   //   //         })
//   //   //         .catch((err) => {
//   //   //           console.error(err);
//   //   //         });
//   //   //     }
//   //   //   ).catch(
//   //   //     (err) => {
//   //   //       alert(err); // i am invoking only this part
//   //   //     }
//   //   //   );
//   //   // }).catch(error => {
//   //   //   alert(error);
//   //   // });
//   // }).catch(error => {
//   //   alert(error);
//   // });

//   console.log(csv);
// }

}
