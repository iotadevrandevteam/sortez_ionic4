import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationplatPage } from './reservationplat.page';

describe('ReservationplatPage', () => {
  let component: ReservationplatPage;
  let fixture: ComponentFixture<ReservationplatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationplatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationplatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
