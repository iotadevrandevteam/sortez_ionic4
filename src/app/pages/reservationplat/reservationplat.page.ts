import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-reservationplat',
  templateUrl: './reservationplat.page.html',
  styleUrls: ['./reservationplat.page.scss'],
})
export class ReservationplatPage implements OnInit {
  msg:any;
  idcom:any;
  data;
  server = 'https://www.randawilly.ovh/';
  platdata: any;
  solde;
  nbrepers;
  nbreplat;
  constructor(
      public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataserv: DataService,
  ) { 
      this.platdata= this.dataserv.getReseravationPlat();
      this.platdata = JSON.parse(this.platdata);
      this.msg = this.platdata.plat;
      alert(JSON.stringify( this.msg));
      for(let liste of this.msg){
        this.solde=parseInt(liste.nbre_plat_propose);
        this.nbrepers +=parseInt(liste.nbre_pers_reserved);
        this.nbreplat +=parseInt(liste.nbre_platDuJour);
      }
  }

  ngOnInit() {
  }
  infoconsoPage(iduser,login){
    let url=this.server+'sortez_pro/sortez_pro_mobile/get_infoconso_new/';
    let body = {id_client:iduser,login:login};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
    this.http.post(url, body, headers).then(data => {
            console.log(data);
            this.dataserv.setData(data.data);
            this.navCtrl.navigateForward('infoconso');
            // this.navCtrl.push(InfoconsoPage,{data});
          }, error => {
             console.log(error);
             alert("Une erreur s'est produite!");
          });
  }
  client_details(idcom,idclientdcom){
    let login=idcom;
    let url=this.server+'sortez_pro/sortez_pro_mobile/get_client_detail_new/';
    let body = {id_client:idclientdcom};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body,headers)
        .then(data => {
            console.log(data);
            this.dataserv.setData(data.data);
            alert(JSON.stringify(data.data));
            this.navCtrl.navigateForward('clientdetail');
            // this.navCtrl.push(ClientdetailPage,{data,login});
          }, error => {
             console.log(error);
             alert("Une erreur s'est produite!");
          });
      }
      sendsms(num){
      
      }
      sendmsg(num,nom,ville,numcom){       
        // this.navCtrl.push(SendsmsPage,{num,nom,ville,numcom});
      }
      filtre_date_demande(){
        this.data.plat = this.data.plat_date_demande;
      }

}
