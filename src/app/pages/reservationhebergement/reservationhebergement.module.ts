import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReservationhebergementPage } from './reservationhebergement.page';

const routes: Routes = [
  {
    path: '',
    component: ReservationhebergementPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReservationhebergementPage]
})
export class ReservationhebergementPageModule {}
