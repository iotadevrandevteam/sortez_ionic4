import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationhebergementPage } from './reservationhebergement.page';

describe('ReservationhebergementPage', () => {
  let component: ReservationhebergementPage;
  let fixture: ComponentFixture<ReservationhebergementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationhebergementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationhebergementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
