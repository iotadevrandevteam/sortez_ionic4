import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-reservationhebergement',
  templateUrl: './reservationhebergement.page.html',
  styleUrls: ['./reservationhebergement.page.scss'],
})
export class ReservationhebergementPage implements OnInit {
  res:any;
  msg:any;
  reservData:any;
  datacom;
  idcom;
  server = 'https://www.randawilly.ovh/';
  constructor(
    public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataserv: DataService,
  ) { 
      this.reservData= this.dataserv.getReseravationPlat();
      this.reservData = JSON.parse(this.reservData);
      this.msg = Array.of(this.reservData);
      this.datacom = this.dataserv.getDataCommercant(); //infocom
  		this.datacom = JSON.parse(this.datacom);
  		this.idcom = this.datacom.json.IdCommercant;
  }

  ngOnInit() {
  }
  filtre_date_demande(){
    this.msg.sejour=this.msg.sejour_date_demande;
  }
  filtre_date_debut(){
    this.msg.sejour=this.msg.sejour_date_debut;
  }
  sendsmsOne(num){
    // this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS);
    // this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE);
   var tel= num.replace(" ", '');
  //  this.sms.send(tel, 'votre message');
  }
  async delete_sejour(id) {
    // let loading = await this.loadingCtrl.create({
    //   message: 'Traitement...'
    // });
    // loading.present();
    // const confirm = this.alertCtrl.create({
    //   title: 'Confirmation',
    //   message: 'Voulez-vous supprimer cette réservation de la liste ?',
    //   buttons: [
    //     {
    //       text: 'Non',
    //       handler: () => {
    //         console.log('Disagree clicked');
    //         loading.dismiss();
    //       }
    //     },
    //     {
    //       text: 'Oui',
    //       handler: () => {
    //         let url = this.server+'sortez_pro/sortez_pro_mobile/delete_res_sejour/';
    //         let body = JSON.stringify({ idres: id });
    //         console.log(body);
    //         const headers = {"Content-Type":"application/json"};
    //           this.http.post(url, body, { headers: headers })
    //             .then(datas => {
    //               console.log(datas);
    //               // if(datas =='ok'){
    //               //   const alert = this.alertCtrl.create({
    //               //     title: 'Succees !',
    //               //     subTitle: 'La liste disparaitra au prochain visite de ce page!',
    //               //     buttons: ['OK']
    //               //   });
    //               //   alert.present();
    //               //   loading.dismiss();
    //               // }
    //               if(datas =='ok'){
    //                 alert('La liste disparaitra au prochain visite de ce page!');
    //               }
    //             }, error => {
    //               console.log("erreur");
    //               alert("Une erreur s'est produite!");
    //               loading.dismiss();
    //             });
    //       }
    //     }
    //   ]
    // });
    // confirm.present();
  }
  sendmsg(num,nom,ville,numcom){   
    // alert(num);     
    // this.navCtrl.push(SendsmsPage,{num,nom,ville,numcom});
  }

}
