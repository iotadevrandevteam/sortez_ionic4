import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesAjoutplatPage } from './pages-ajoutplat.page';

describe('PagesAjoutplatPage', () => {
  let component: PagesAjoutplatPage;
  let fixture: ComponentFixture<PagesAjoutplatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesAjoutplatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesAjoutplatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
