import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-modifbonplan',
  templateUrl: './modifbonplan.page.html',
  styleUrls: ['./modifbonplan.page.scss'],
})
export class ModifbonplanPage implements OnInit {
  msg:any;
  idcom:any;
  data;
  server = 'https://www.randawilly.ovh/';
  updatedata: any;
  constructor(
      public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataCom: DataService,
  ) { 
      this.updatedata = this.dataCom.getData()
      this.msg = JSON.parse(this.updatedata);
      // this.msg = Array.of(this.updatedata);
      alert(JSON.stringify(this.msg))
  }

  ngOnInit() {
  }
  async SaveBonplan(activ_bp_multiple_nbmax,activ_bp_multiple_qttprop,activ_bp_multiple_prix,activ_bp_unique_date_visit,activ_bp_unique_nbmax,activ_bp_unique_qttprop,activ_bp_unique_prix,activ_bp_multiple_reserv_regler,activ_bp_multiple_date_visit,bp_multiple_nbmax,bp_multiple_qttprop,bp_multiple_eco,bp_multiple_value,bp_multiple_prix,bp_multiple_date_fin,bp_multiple_date_debut,activ_bp_unique_reserv_regler,bp_unique_nbmax,bp_unique_qttprop,bp_unique_eco,bp_unique_value,bp_unique_date_fin,bp_unique_prix,bp_unique_date_debut,bp_simple_eco,bp_simple_value,bp_simple_prix,bonplan_date_fin,bonplan_date_debut,bonplan_commercant_id,bonplan_id,bonplan_titre,bonplan_texte,bonplan_type){
    let loading = await this.loadingCtrl.create({
      message: 'Traitement...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    if(activ_bp_multiple_nbmax==true){
      activ_bp_multiple_nbmax=1;
    }else{activ_bp_multiple_nbmax=0;}

    if(activ_bp_multiple_qttprop==true){
      activ_bp_multiple_qttprop=1;
    }else{activ_bp_multiple_qttprop=0;}

    if(activ_bp_multiple_prix==true){
      activ_bp_multiple_prix=1;
    }else{activ_bp_multiple_prix=0;}

    if(activ_bp_unique_date_visit==true){
      activ_bp_unique_date_visit=1;
    }else{activ_bp_unique_date_visit=0;}

    if(activ_bp_unique_nbmax==true){
      activ_bp_unique_nbmax=1;
    }else{activ_bp_unique_nbmax=0;}

    if(activ_bp_unique_qttprop==true){
      activ_bp_unique_qttprop=1;
    }else{activ_bp_unique_qttprop=0;}

    if(activ_bp_unique_prix==true){
      activ_bp_unique_prix=1;
    }else{activ_bp_unique_prix=0;}

    if(activ_bp_multiple_reserv_regler==true){
      activ_bp_multiple_reserv_regler=1;
    }else{activ_bp_multiple_reserv_regler=0;}

    if(activ_bp_multiple_date_visit==true){
      activ_bp_multiple_date_visit=1;
    }else{activ_bp_multiple_date_visit=0;}
    //////////////////////////////////
    if(!bp_multiple_nbmax){
      bp_multiple_nbmax=0;
    }
    if(!bp_multiple_qttprop){
      bp_multiple_qttprop="";
    }
    if(!bp_multiple_eco){
      bp_multiple_eco="";
    }
    if(!bp_multiple_value){
      bp_multiple_value="";
    }
    if(!bp_multiple_prix){
      bp_multiple_prix="";
    }
    if(!bp_multiple_date_fin){
      bp_multiple_date_fin="";
    }
    if(!bp_multiple_date_debut){
      bp_multiple_date_debut="";
    }
    if(!activ_bp_unique_reserv_regler){
      activ_bp_unique_reserv_regler="";
    }
    if(!bp_unique_nbmax){
      bp_unique_nbmax=0;
    }
    if(!bp_unique_qttprop){
      bp_unique_qttprop="";
    }
    if(!bp_unique_eco){
      bp_unique_eco="";
    }
    if(!bp_unique_value){
      bp_unique_value="";
    }
    if(!bp_unique_date_fin){
      bp_unique_date_fin="";
    }
    if(!bp_unique_prix){
      bp_unique_prix="";
    }
    if(!bp_unique_date_debut){
      bp_unique_date_debut="";
    }
    if(!bp_simple_eco){
      bp_simple_eco="";
    }
    if(!bp_simple_value){
      bp_simple_value="";
    }
    if(!bp_simple_prix){
      bp_simple_prix="";
    }
    if(!bonplan_date_fin){
      bonplan_date_fin="";
    }
    if(!bonplan_date_debut){
      bonplan_date_debut="";
    }
    if(!bonplan_commercant_id){
      bonplan_commercant_id="";
    }
    if(!bonplan_id){
      bonplan_id="";
    }
    if(!bonplan_titre){
      bonplan_titre="";
    }
    if(!bonplan_texte){
      bonplan_texte="";
    }
    if(!bonplan_type){
      bonplan_type="";
    }

    



    let url=this.server+'sortez_pro/sortez_pro_mobile/modifBonplan_new/';
    let body = {activ_bp_multiple_nbmax:activ_bp_multiple_nbmax,activ_bp_multiple_qttprop:activ_bp_multiple_qttprop,activ_bp_multiple_prix:activ_bp_multiple_prix,activ_bp_unique_date_visit:activ_bp_unique_date_visit,activ_bp_unique_nbmax:activ_bp_unique_nbmax,activ_bp_unique_qttprop:activ_bp_unique_qttprop,activ_bp_unique_prix:activ_bp_unique_prix,activ_bp_multiple_reserv_regler:activ_bp_multiple_reserv_regler,activ_bp_multiple_date_visit:activ_bp_multiple_date_visit,bp_multiple_nbmax:bp_multiple_nbmax,bp_multiple_qttprop:bp_multiple_qttprop,bp_multiple_eco:bp_multiple_eco,bp_multiple_value:bp_multiple_value,bp_multiple_prix:bp_multiple_prix,bp_multiple_date_fin:bp_multiple_date_fin,bp_multiple_date_debut:bp_multiple_date_debut,activ_bp_unique_reserv_regler:activ_bp_unique_reserv_regler,bp_unique_nbmax:bp_unique_nbmax,bp_unique_qttprop:bp_unique_qttprop,bp_unique_eco:bp_unique_eco,bp_unique_value:bp_unique_value,bp_unique_date_fin:bp_unique_date_fin,bp_unique_prix:bp_unique_prix,bp_unique_date_debut:bp_unique_date_debut,bp_simple_eco:bp_simple_eco,bp_simple_value:bp_simple_value,bp_simple_prix:bp_simple_prix,bonplan_date_fin:bonplan_date_fin,bonplan_date_debut:bonplan_date_debut,bonplan_commercant_id:bonplan_commercant_id,bonplan_id:bonplan_id,bonplan_titre:bonplan_titre,bonplan_texte:bonplan_texte,bonplan_type:bonplan_type};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body,headers).then(data => {
            console.log(data);
            let toast = this.toastCtrl.create({
              message: 'Bon plan enregistré avec succès',
              duration: 3000,
              position: 'top'
            }).then((toast) => {
              toast.present();
            });
            loading.dismiss();
            this.navCtrl.navigateForward('parabp');
           // this.navCtrl.pop();
          }, error => {
             alert("Champ incorrecte ou manquant")
          });
  }

}
