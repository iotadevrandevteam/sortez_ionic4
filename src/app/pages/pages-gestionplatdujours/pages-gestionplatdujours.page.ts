import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-pages-gestionplatdujours',
  templateUrl: './pages-gestionplatdujours.page.html',
  styleUrls: ['./pages-gestionplatdujours.page.scss'],
})
export class PagesGestionplatdujoursPage implements OnInit {
  res:any;
  msg:any;
  comdata:any;
  idcom;
  server = 'https://www.randawilly.ovh/';
  constructor(
      public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataserv: DataService,
  ) { 
      this.comdata = this.dataserv.getDataCommercant(); //infocom
  		this.comdata = JSON.parse(this.comdata);
  		this.idcom = this.comdata.json.IdCommercant;
  }

  ngOnInit() {
    let msg = this.idcom;
    let url = this.server+'sortez_pro/sortez_pro_mobile/liste_plat_new/';
    let body = { idcommercant: msg };
    console.log(body);
    // alert(JSON.stringify(body));
    const headers ={"Content-Type":"application/json"};
      this.http.post(url, body, headers ).then(data => {
          console.log(data.data[0]);
          console.log('data.data[0]');
          if (data == null) { alert("Pas de donnée") }
          else if(data.data[0] != null && data.data[0] != undefined && data.data[0] != '' ){
            this.res=JSON.parse(data.data);
            console.log(this.res); 
            // alert(JSON.stringify(this.res));  
            console.log("id ion auth");   
            // console.log(this.res.IdUsers_ionauth);              
          }

        }, error => {
          let toast = this.toastCtrl.create({
            showCloseButton: true,
            closeButtonText: 'ok',
            message: "Vous n'avez pas encore de client",
            position: 'middle'
          }).then((toast)=>{
            toast.present();
          });
        });
  }
  async delete_plat(id){
    
    let loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    loading.present();
    let url = this.server+'sortez_pro/sortez_pro_mobile/delete_plat_by_id/';
    let body = { id: id };
    const headers = {"Content-Type":"application/json"};
      this.http.post(url, body, headers).then(data => {
            loading.dismiss();
            this.ngOnInit();
            alert('Plat supprimé');

        }, error => {
          loading.dismiss();
          alert('Erreur de suppression');
        });
  }

  
  update_plat(idcommercant){
    // this.navCtrl.push(PagesUpdateplatPage, { idcommercant });
  }

  add_plat(){
    let idcom=this.msg;
    // this.navCtrl.push(PagesAjoutplatPage, { idcom });
  }

  set_photo(id){
    // this.fileChooser.open()
    // .then(uri =>  this.upload(uri,id))
    // .catch(e => console.log(e));
  }

    async upload(uri,id) {
    let loading = await this.loadingCtrl.create({
      message: 'Chargement...'
    });
    loading.present();
    let lettre_aleatoire='photo_'+id;
  //   let options: FileUploadOptions = {
  //     fileKey: 'file',
  //     fileName: lettre_aleatoire+'-'+this.msg+'-p.jpg',
  //     headers: {}
  //  }
   //alert(lettre_aleatoire+'-'+this.msg+'-p.jpg');
    // this.fileTransfer.upload(uri, 'https://www.sortez.org/sortez_pro/sortez_pro_mobile/save_plat_image/', options)
    // .then((data) => {
      let url=this.server+'sortez_pro/sortez_pro_mobile/save_plat_data_image/';
      //let body = JSON.stringify({id_plat:id,idcom:this.msg,nom_photo:lettre_aleatoire+'.jpg'});
      let body = {id_plat:id,idcom:this.msg,nom_photo:lettre_aleatoire+'-'+this.msg+'-p.jpg'};
      console.log(body);
      const headers = {"Content-Type":"application/json"};
      return new Promise(resolve => {
        this.http.post(url, body, headers).then(data => {
              console.log(data);
              loading.dismiss();
              alert('Image importé');
              this.relance();
              this.ngOnInit();
            }, error => {
              loading.dismiss();
              alert('erreure d\'enregistrement');
            });
          });
    // }, (err) => {
    //   alert(JSON.stringify(err));
    //   console.log('Erreur');
    //     })
 }

 async delete_photo(id){
  // alert("suppre plat = "+id);
  let loading = await this.loadingCtrl.create({
    message: 'Chargement...'
  });
  loading.present();
  let url=this.server+'sortez_pro/sortez_pro_mobile/delete_plat_data_image/';
  let body = {id_plat:id,idcom:this.msg};
  //alert(body);
  console.log(body);
  const headers = {"Content-Type":"application/json"};
  this.http.post(url, body, headers).then(data => {
          console.log(data);
          loading.dismiss();
          alert('Image importé');
          this.ngOnInit();
        }, error => {
          loading.dismiss();
          //alert('erreure d\'enregistrement');
          alert('Image supprimé');
          this.relance();
        });
 }
 relance(){
  let idcom = this.idcom;
  let url = this.server+'sortez_pro/sortez_pro_mobile/liste_plat/';
  let body = { idcommercant: idcom };
  console.log(body);
  const headers = {"Content-Type":"application/json"};
    this.http.post(url, body, headers)
      .then(data => {
        console.log(data);
        if (data == null) { alert("Pas de donnée") }
        else {
          
          this.res=data;
          console.log(data);   
          // console.log("id ion auth");   
          // console.log(data[0].IdUsers_ionauth);    
          console.log("photo");   
          console.log(this.res.photo);           
        }

      }, error => {
        let toast = this.toastCtrl.create({
          showCloseButton: true,
          closeButtonText: 'ok',
          message: "Vous n'avez pas encore de client",
          position: 'middle'
        }).then((toast)=>{
          toast.present();
        });
      });
}
}
