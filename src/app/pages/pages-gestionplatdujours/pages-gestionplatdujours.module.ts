import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagesGestionplatdujoursPage } from './pages-gestionplatdujours.page';

const routes: Routes = [
  {
    path: '',
    component: PagesGestionplatdujoursPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagesGestionplatdujoursPage]
})
export class PagesGestionplatdujoursPageModule {}
