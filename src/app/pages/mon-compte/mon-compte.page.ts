import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-mon-compte',
  templateUrl: './mon-compte.page.html',
  styleUrls: ['./mon-compte.page.scss'],
})
export class MonComptePage implements OnInit {

  server = 'https://www.randawilly.ovh/';
  infocom: any;
	idcom: any;
	comdata: any;
  constructor(
    public http: HTTP,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public storageService: StorageService,
    public dataCom: DataService,) { 
      this.comdata = this.dataCom.getDataCommercant();
  		this.comdata = JSON.parse(this.comdata);
  		this.idcom = this.comdata.json.IdCommercant;
      this.infocom = Array.of(this.comdata);
      // alert(JSON.stringify(this.infocom));
    }

  ngOnInit() {
  }
  async ModifAccount(IdCommercant,Nom,Prenom,CodePostal,Civilite,TelFixe,TelMobile,Email){

    // alert(IdCommercant);
    // alert(Nom);
    // alert(Prenom);
    // alert(CodePostal);
    // alert(Civilite);
    // alert(TelFixe);
    // alert(TelMobile);
    // alert(Email);
    let url= this.server+'sortez_pro/sortez_pro_mobile/ModifAccount2/';
    // alert(url)
    let body = {IdCommercant:IdCommercant,Nom:Nom,Prenom:Prenom,CodePostal:CodePostal,Civilite:Civilite,TelFixe:TelFixe,TelMobile:TelMobile,Email:Email};
    console.log(body);
    // alert(JSON.stringify(body))
    var headers = { "Content-Type": 'application/json' };
        this.http.post(url,body, headers).then(data => {
          console.log((data)); 
          // alert(JSON.stringify(data))
          const toast = this.toastCtrl.create({
              message: 'Les modifications de votre compte sont enregistrées avec succès',
                  duration: 2000,
                  position: 'top'
            }).then((toastData)=>{
              toastData.present();
          });
        }, error => {
          alert(error);
          console.log(error);
        });
    // var headers = new Headers({'Content-Type': 'application/json'});
    // this.http.post(url, body, {headers: headers}).then(data => {
    //         console.log(data);
    //         const toast = this.toastCtrl.create({
    //           message: 'Les modifications de votre compte sont enregistrées avec succès',
    //               duration: 2000,
    //               position: 'top'
    //         }).then((toastData)=>{
    //           toastData.present();
    //         });

    //       }, error => {
    //          console.log("erreur")
    //          alert("Une erreur s'est produite")
    //       });
  }

}
