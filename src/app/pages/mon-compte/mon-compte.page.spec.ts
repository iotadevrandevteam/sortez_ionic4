import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonComptePage } from './mon-compte.page';

describe('MonComptePage', () => {
  let component: MonComptePage;
  let fixture: ComponentFixture<MonComptePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonComptePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonComptePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
