import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-detail-table',
  templateUrl: './detail-table.page.html',
  styleUrls: ['./detail-table.page.scss'],
})
export class DetailTablePage implements OnInit {
  res:any;
  msg:any;
  reservData:any;
  idcom;
  server = 'https://www.randawilly.ovh/';
  constructor(
    public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
      public dataserv: DataService,
      public alertCtrl:AlertController,
  ) {
      this.reservData= this.dataserv.getReseravationPlat();
      this.reservData = JSON.parse(this.reservData);
      this.msg = Array.of(this.reservData);
      this.reservData = this.dataserv.getDataCommercant(); //infocom
  		this.reservData = JSON.parse(this.reservData);
  		this.idcom = this.reservData.json.IdCommercant;
   }

  ngOnInit() {
  }
  filtre_date_demande(){
    this.msg.table=this.msg.table_date_demande;
  }
  filtre_date_debut(){
    this.msg.table=this.msg.table_date_debut;
  }
  sendsmsOne(num){
  //   this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS);
  //   this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE);
  //  var tel= num.replace(" ", '');
  //  this.sms.send(tel, 'votre message');
  }
  async delete_table(id) {
    let loading = await this.loadingCtrl.create({
      message: 'Traitement...'
    });
    loading.present();
    const confirm = this.alertCtrl.create({
      header: 'Alert',
      subHeader: 'Confirmation',
      message: 'Voulez-vous supprimer cette réservation de la liste ?',
      buttons: [
        {
          text: 'Non',
          handler: () => {
            console.log('Disagree clicked');
            loading.dismiss();
          }
        },
        {
          text: 'Oui',
          handler: () => {
            let url = this.server+'sortez_pro/sortez_pro_mobile/delete_res_table/';
            let body = { idres: id };
            console.log(body);
            const headers = {"Content-Type":"application/json"};
              this.http.post(url, body, { headers: headers })
                .then(datas => {
                  console.log(datas);
                  // if(datas=='ok'){
                  //   const alert = this.alertCtrl.create({
                  //     title: 'Succees !',
                  //     subTitle: 'La liste disparaitra au prochain visite de ce page!',
                  //     buttons: ['OK']
                  //   }).then((alert)=>{
                  //     alert.present();
                  //   })
                  //   loading.dismiss();
                  // }
                }, error => {
                  console.log("erreur");
                  alert("Une erreur s'est produite!");
                  loading.dismiss();
                });
          }
        }
      ]
    }).then((confirm)=>{
      confirm.present();
    })
  }
  sendmsg(num,nom,ville,numcom){   

    // this.navCtrl.push(SendsmsPage,{num,nom,ville,numcom});
  }

}
