import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-pages-updateplat',
  templateUrl: './pages-updateplat.page.html',
  styleUrls: ['./pages-updateplat.page.scss'],
})
export class PagesUpdateplatPage implements OnInit {
  res:any;
  idplat:any;
  server = 'https://www.randawilly.ovh/';
  comdata:  any;
  constructor(
      public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataCom: DataService,
  ) { 
    this.comdata = this.dataCom.getReseravationPlat() //infocom
    this.comdata = JSON.parse(this.comdata);
    alert(JSON.stringify(this.comdata));
    // this.idplat = this.comdata.json.IdCommercant;
  }

  ngOnInit() {
    let idplatjour=  this.idplat;  
    let url = this.server +'sortez_pro/sortez_pro_mobile/update_by_idplat/';
    let body = { idplat: idplatjour };
    console.log(body);
    const headers = {"Content-Type":"application/json"};
      this.http.post(url, body,headers).then(data => {
          console.log(data);
          if (data == null) { alert("Pas de donnée") }
          else {
            this.res=data.data;
            //alert(this.res);
            console.log("resultat data"); 
            console.log(data);       
            alert(JSON.stringify(this.res))    
          }

        }, error => {
          let toast = this.toastCtrl.create({
            showCloseButton: true,
            closeButtonText: 'ok',
            message: "Désolé, une erreur c'est produit!",
            position: 'middle'
          }).then((toast)=>{
              toast.present();
          });
        });
  }
  async save_update_plat(idplat,date_debut,date_fin,heure_debut,heure_fin,txt_plat,prix_plat,nbr_plat){
    let loading = await this.loadingCtrl.create({
      message: 'Traitement...'
    });
  
    loading.present();
  
    setTimeout(() => {
      loading.dismiss();
    }, 2000);

    if(!date_debut){
      date_debut="";
    }
    if(!date_fin){
      date_fin="";
    }
    if(!heure_debut){
      heure_debut="";
    }
    if(!heure_fin){
      heure_fin="";
    }
    if(!txt_plat){
      txt_plat="";
    }
    if(!prix_plat){
      prix_plat="";
    }
    if(!prix_plat){
      prix_plat="";
    }
    let url=this.server + 'sortez_pro/sortez_pro_mobile/update_plat/';
    let body = {idplat:idplat,date_debut:date_debut,date_fin:date_fin,heure_debut:heure_debut,heure_fin:heure_fin,txt_plat:txt_plat,prix_plat:prix_plat,nbr_plat:nbr_plat};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
    this.http.post(url, body,headers).then(data => {
            console.log(data);
            let toast = this.toastCtrl.create({
              message: 'Plat du jour modifié avec succès',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            });
            this.navCtrl.pop();
            //this.navCtrl.push(PagesGestionplatdujoursPage, { idcommercant });  
           // this.navCtrl.pop();
          }, error => {
             alert("Champ incorrecte ou manquant")
          });
  }

}
