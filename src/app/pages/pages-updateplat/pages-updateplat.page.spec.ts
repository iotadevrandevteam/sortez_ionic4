import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesUpdateplatPage } from './pages-updateplat.page';

describe('PagesUpdateplatPage', () => {
  let component: PagesUpdateplatPage;
  let fixture: ComponentFixture<PagesUpdateplatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesUpdateplatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesUpdateplatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
