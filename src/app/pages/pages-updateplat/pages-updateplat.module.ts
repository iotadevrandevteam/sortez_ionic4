import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagesUpdateplatPage } from './pages-updateplat.page';

const routes: Routes = [
  {
    path: '',
    component: PagesUpdateplatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagesUpdateplatPage]
})
export class PagesUpdateplatPageModule {}
