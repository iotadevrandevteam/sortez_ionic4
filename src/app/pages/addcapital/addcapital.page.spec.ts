import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcapitalPage } from './addcapital.page';

describe('AddcapitalPage', () => {
  let component: AddcapitalPage;
  let fixture: ComponentFixture<AddcapitalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcapitalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcapitalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
