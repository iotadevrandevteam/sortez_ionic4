import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-addcapital',
  templateUrl: './addcapital.page.html',
  styleUrls: ['./addcapital.page.scss'],
})
export class AddcapitalPage implements OnInit {
  res:any;
  msg:any;
  dataremiseData:any;
  idcom;
  data:any;
  server = 'https://www.randawilly.ovh/';
  constructor(
    public http: HTTP,
      public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
      public dataserv: DataService,
      public alertCtrl:AlertController,
  ) { 
      this.dataremiseData = this.dataserv.getData(); //infocom
      this.dataremiseData = JSON.parse(this.dataremiseData);
      this.data = Array.of(this.dataremiseData);
  }

  ngOnInit() {
  }
  Validatecapital(id_ionauth,id_commercant,is_activ,description,date_debut,date_fin,montant){
    if(is_activ==true){
      is_activ=1;
    }else{is_activ=null;}
    let url=this.server+'sortez_pro/sortez_pro_mobile/editcapital/';
    let body = {id_ionauth:id_ionauth,id_commercant:id_commercant,is_activ:is_activ,description:description,date_debut:date_debut,date_fin:date_fin,montant:montant};
    console.log(body);
    const headers = {"Content-Type":"application/json"};
        this.http.post(url, body, headers)
        .then(data => {
            console.log(data);
            const toast = this.toastCtrl.create({
              message: 'Succès',
              duration: 3000,
              position: 'top'
            }).then((toast)=>{
              toast.present();
            })
          }, error => {
             alert("Champ incorrecte ou manquant")
          });
  }

}
