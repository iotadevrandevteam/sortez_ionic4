import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	items: Item[] = [];
	newItem: Item = <Item>{};
	name: string;
	identit: string;
	passwor: string;
	infocom: any;
	rmchecked;
	data_user;
	// server = 'https://www.sortez.org/';
	server = 'https://www.randawilly.ovh/';
	constructor(
		public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataCom: DataService,
	) { }

	ngOnInit() {

	}

	gotoforgottenpage(){
	    this.navCtrl.navigateForward('forgottenpass');
	}

	ionViewDidLoad() {
	    this.storageService.getItems().then(items => {
		  	if(items != null && items.length != 0 ){
		    	this.data_user = items;
		    	this.identit = items[0].login;
		    	this.passwor = items[0].pass;
		    	this.rmchecked == true;
		  	}else{
		    	this.data_user = null;
		  	}
	    });
	}

	async login(identit, passwor){
		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();
		let url = this.server+'auth/login_sortez_pro2/';
		let alllogin = identit+"we_will_explode_by_this"+passwor
		let body = { loginandpassword : alllogin};
		// alert(JSON.stringify(body))
	    const headers = { "Content-Type": 'application/json' };
	    let test = new Promise(resolve => {
	    	this.http.post(url, body, headers)
	    	.then(data => {
				// alert(JSON.stringify(data.data))
	    		if(data.data == null || data.data === ""){
	    			const toast = this.toastCtrl.create({
					    message: 'Mot de passe ou Adresse E-mail incorrecte ou ce compte n\'existe pas.Assurez-vous que ce compte est un compte commercant',
			            duration: 2000,
			            position: 'top'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('error ato')
				    loading.dismiss()
	    		}else{
	    			let url = this.server+'sortez_pro/sortez_pro_mobile/get_login_by_id_test/'
	    			let body = { login: identit };
	    			const headers = { "Content-Type": 'application/json' };
	    			return new Promise(resolve => {
	    				this.http.post(url, body, headers)
	    				.then(infocom => {
	    					loading.dismiss()
	    					this.dataCom.setDataCommercant(infocom.data)
	    					this.dataCom.setSession(true)
	    					this.dataCom.setMail(identit)
	    					this.navCtrl.navigateForward('home')
	    				})
	    				.catch(error => {
	    					console.log('error2: '+error)
	    					loading.dismiss()
	    				})
	    			})
	    		}
	    	})
	    	.catch(error => {
				console.log(JSON.stringify(error));
	    		const toast = this.toastCtrl.create({
				    message: 'Mot de passe ou Adresse E-mail incorrecte ou ce compte n\'existe pas.Assurez-vous que ce compte est un compte commercant',
		            duration: 2000,
		            position: 'top'
			    }).then((toastData)=>{
				    toastData.present();
			    });
			    loading.dismiss()
			    console.log('error debut:'+error)
	    	})
	    })
	}

	save_data(rmchecked,login,password){
		if(rmchecked == true && this.data_user == null){
		    this.newItem.login =login;
		    this.newItem.pass=password;
		    this.storageService.addItem(this.newItem).then(item => {
		     
		    });
		}else if(rmchecked == true && this.data_user != null){
		    this.newItem.login =login;
		    this.newItem.pass=password;
		    this.storageService.updateItem(this.newItem).then(item => {
		    
		    });
		}else if(rmchecked == false && this.data_user != null){
		    this.storageService.deleteItem(this.data_user[0].login).then(item => {
		      
		    });
		}
	}

}
