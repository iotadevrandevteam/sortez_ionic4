import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

	// server = 'https://www.sortez.org/';
	server = 'https://www.randawilly.ovh/';
	mail: any;
	infocom: any;
	idcom: any;
	comdata: any;
	session;
  	constructor(
  		public http: HTTP,
	    public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
	    public dataCom: DataService,
  	) { 
  		this.comdata = this.dataCom.getDataCommercant() //infocom
  		this.comdata = JSON.parse(this.comdata)
  		this.idcom = this.comdata.json.IdCommercant
  		this.infocom = Array.of(this.comdata)

  		this.session = this.dataCom.getSession()
  		this.mail = this.dataCom.getMail()


  		if(this.session != false && this.mail != null){
  			const toast = this.toastCtrl.create({
			    message: 'Vous êtes connecté à votre compte \'Platinium\'',
	            duration: 2000,
	            position: 'top'
		    }).then((toastData)=>{
			    toastData.present();
		    });
		    console.log(this.session)
		    console.log(this.mail)
  		}else{
  			const toast = this.toastCtrl.create({
			    message: 'Vous n\' êtes pas connecté à votre compte privicarte',
	            duration: 2000,
	            position: 'top'
		    }).then((toastData)=>{
			    toastData.present();
		    });
		    console.log(this.session)
		    console.log(this.mail)
		    this.navCtrl.navigateForward('login')
  		}

  	}

  	ngOnInit() {

  	}

  	async logout(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();
  		this.dataCom.setSession(false)
  		this.dataCom.setMail(null)
  		this.dataCom.setDataCommercant(null)
  		this.dataCom.setListClient(null)
  		this.dataCom.setListReservation(null)
  		this.dataCom.setReservationPLat(null)
  		loading.dismiss()
  		this.navCtrl.navigateForward('login')
  	}

  	MonComptePages(){
  		this.navCtrl.navigateForward('mon-compte')
  	}

  	async ClientListPages(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();
  			let mail = this.mail 
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_clients_new/'
	  		let body = { login: mail }
	  		const headers = { "Content-Type": 'application/json' }
	  		return new Promise(resolve => {
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setListClient(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('client-list')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Vous n'avez pas encore de client",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur client: '+error)
	  			})
	  		})
  	}

  	ParametrePage(){
  		this.navCtrl.navigateForward('parametre')
  	}

  	async ReservationPages(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();
	    	let mail = this.mail;
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_reservationionic_new/';
	  		let body = { login: mail };
	  		const headers = { "Content-Type": 'application/json' }
	  		this.http.post(url, body, headers).then(data => {
	  				if(data == null){
	  					alert("Pas de donnée");
	  				}else{
	  					this.dataCom.setListReservation(data.data);
	  					loading.dismiss();
	  					this.navCtrl.navigateForward('reservation');
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Vous n'avez pas encore de reservation",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
					console.log('erreur reservation: '+error)
					loading.dismiss()
	  			});
  	}

  	ListValidPages(){
  		this.navCtrl.navigateForward('list-valid')
  	}

  	async SendsmsPage(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();

  		if(this.dataCom.getListClient == null){
  			let mail = this.mail 
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_clients_new/'
	  		let body = { login: mail }
	  		const headers = { "Content-Type": 'application/json' }
	  		return new Promise(resolve => {
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setListClient(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('sendsms')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Vous n'avez pas encore de client",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur sms: '+error)
	  			})
	  		})
  		}else{
  			loading.dismiss()
  			this.navCtrl.navigateForward('sendsms')
  		}
  	}

  	async RestaurantPage(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();

  			let mail = this.mail 
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_reservationionic_new/'
	  		let body = { login: mail }
	  		const headers = { "Content-Type": 'application/json' }
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setReservationPLat(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('restaurant')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Une erreur s'est produite",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur restaurant: '+error)
	  			})
  	}

  	async detail_res_plat(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'chargement...'
	    });
	    loading.present();

  			let mail = this.mail ;
			  let url = this.server+'sortez_pro/sortez_pro_mobile/liste_reservation_plat_new/';
			  let body = { login: mail }
			//   alert(url);
			//   alert(JSON.stringify(body));
	  		const headers = { "Content-Type": 'application/json' }
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setReservationPLat(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('reservationplat')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Une erreur s'est produite",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur plat: '+error)
	  			})
  	}

  	async detail_res_sejour(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();

  		
  			let mail = this.mail 
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_reservation_plat_new/'
			  let body = { login: mail }
			//   alert(url);
			//   alert(JSON.stringify(body));
	  		const headers = { "Content-Type": 'application/json' }
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setReservationPLat(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('reservationhebergement')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Une erreur s'est produite",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur hebergement: '+error)
	  			})
  	}

  	async detail_table(){
  		let loading = await this.loadingCtrl.create({
	      	message: 'Connexion...'
	    });
	    loading.present();

  			let mail = this.mail 
	  		let url = this.server+'sortez_pro/sortez_pro_mobile/liste_reservation_plat_new/'
			  let body = { login: mail }
			//   alert(url);
			//   alert(JSON.stringify(body));
	  		const headers = { "Content-Type": 'application/json' }
	  		
	  			this.http.post(url, body, headers)
	  			.then(data => {
	  				if(data == null){
	  					alert("Pas de donnée")
	  				}else{
	  					this.dataCom.setReservationPLat(data.data)
	  					loading.dismiss()
	  					this.navCtrl.navigateForward('detail-table')
	  				}
	  			})
	  			.catch(error => {
	  				const toast = this.toastCtrl.create({
	  					showCloseButton: true,
	            		closeButtonText: 'ok',
					    message: "Une erreur s'est produite",
			            // duration: 2000,
			            position: 'middle'
				    }).then((toastData)=>{
					    toastData.present();
				    });
				    console.log('erreur table: '+error)
	  			})
	  		
  	}

  	parabon(){
  		this.navCtrl.navigateForward('parabp')
  	}

  	gestion_plat(){
  		this.navCtrl.navigateForward('pages-gestionplatdujours')
  	}

}


// $value_code = explode('=',$rest_json)[1];
//         $login = urldecode($value_code);