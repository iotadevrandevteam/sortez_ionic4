import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, NavParams } from '@ionic/angular';
import { Platform, ToastController, AlertController, IonList} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { StorageService, Item } from '../../services/storage/storage.service';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: 'app-paracc',
  templateUrl: './paracc.page.html',
  styleUrls: ['./paracc.page.scss'],
})
export class ParaccPage implements OnInit {
  res:any;
  msg:any;
  dataremiseData:any;
  idcom;
  data:any;
  server = 'https://www.randawilly.ovh/';
  constructor(
    public http: HTTP,
      public navCtrl: NavController,
	    public toastCtrl: ToastController,
	    public loadingCtrl: LoadingController,
	    public storageService: StorageService,
      public dataserv: DataService,
      public alertCtrl:AlertController,
  ) { 
      this.dataremiseData = this.dataserv.getData(); //infocom
      this.dataremiseData = JSON.parse(this.dataremiseData);
      this.msg = Array.of(this.dataremiseData);
  }

  ngOnInit() {
  }

}
