import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParaccPage } from './paracc.page';

describe('ParaccPage', () => {
  let component: ParaccPage;
  let fixture: ComponentFixture<ParaccPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParaccPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParaccPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
