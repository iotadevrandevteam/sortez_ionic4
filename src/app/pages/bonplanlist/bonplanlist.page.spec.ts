import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonplanlistPage } from './bonplanlist.page';

describe('BonplanlistPage', () => {
  let component: BonplanlistPage;
  let fixture: ComponentFixture<BonplanlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonplanlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonplanlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
