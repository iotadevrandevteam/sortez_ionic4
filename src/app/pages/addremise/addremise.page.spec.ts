import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddremisePage } from './addremise.page';

describe('AddremisePage', () => {
  let component: AddremisePage;
  let fixture: ComponentFixture<AddremisePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddremisePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddremisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
