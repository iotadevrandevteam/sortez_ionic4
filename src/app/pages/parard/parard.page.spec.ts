import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParardPage } from './parard.page';

describe('ParardPage', () => {
  let component: ParardPage;
  let fixture: ComponentFixture<ParardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
