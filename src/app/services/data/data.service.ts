import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

	data_commercant = null;
	session = false;
	mail = null;
	listClient = null;
	listReservation = null;
	reservationPlat = null;
	idCommercant = null;
	data_valid= null;
	data_get= null;
	dataget= null;
	login = null;
	get_all = null;

  	constructor() { }

  	// data commercant
  	getDataCommercant(){ return this.data_commercant }
  	setDataCommercant(data){ this.data_commercant = data }

  	// session commercant
  	getSession(){ return this.session }
  	setSession(data){ this.session = data }

  	// mail commercant
  	getMail(){ return this.mail }
  	setMail(data){ this.mail = data }

  	// list client
  	getListClient(){ return this.listClient }
  	setListClient(data){ this.listClient = data }

  	// list reservation
  	getListReservation(){ return this.listReservation }
  	setListReservation(data){ this.listReservation = data }

  	// reservation plat
  	getReseravationPlat(){ return this.reservationPlat }
  	setReservationPLat(data){ this.reservationPlat = data }

  	// id commercant
  	getIdCommercant(){ return this.idCommercant }
	  setIdCommercant(data){ this.idCommercant = data }
	  
	  // data valid commercants
  	getvalid(){ return this.data_valid }
	  setvalid(data){ this.data_valid = data }
	
	getData(){ return this.data_get }
	setData(data){this.data_get = data}

	setDataclient(data,login){this.dataget = data; this.login = login}
	getDataclient(){return  this.get_all[this.dataget,this.login]}
}
