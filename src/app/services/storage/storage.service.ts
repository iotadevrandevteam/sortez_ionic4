import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';

export interface Item {
	login: string,
	pass: string,
}

const ITEMS_KEY = 'user_data';

@Injectable({
  providedIn: 'root'
})

export class StorageService {

	private destn: any;

	constructor(
		public http: HTTP, 
		private storage: Storage

	) { }

	addItem(item: Item): Promise<any> {
		return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
		    if (items) {
			    items.push(item);
			    return this.storage.set(ITEMS_KEY, items);
		    } else {
			    return this.storage.set(ITEMS_KEY, [item]);
		    }
		});
	}

	getItems(): Promise<Item[]> {
	  	return this.storage.get(ITEMS_KEY);
	}

	updateItem(item: Item): Promise<any> {
		return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
		    if (!items || items.length === 0) {
		      	return null;
		    }

		    let newItems: Item[] = [];

		    for (let i of items) {
			    if (i.login === item.login) {
			        newItems.push(item);
			    } else {
			        newItems.push(i);
			    }
		    }

		    return this.storage.set(ITEMS_KEY, newItems);
		});
	}

	deleteItem(id: string): Promise<Item> {
	  	return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
		    if (!items || items.length === 0) {
		      	return null;
		    }

		    let toKeep: Item[] = [];

		    for (let i of items) {
		      	if (i.login !== id) {
		        	toKeep.push(i);
		      	}
		    }
		    return this.storage.set(ITEMS_KEY, toKeep);
		});
	}

	public setDestn(destn) {
	  	this.destn = destn;
	}

	getDestn() {
	  	return this.destn;
	}

}
