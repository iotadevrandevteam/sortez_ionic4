import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemisePage } from './remise.page';

describe('RemisePage', () => {
  let component: RemisePage;
  let fixture: ComponentFixture<RemisePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemisePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
