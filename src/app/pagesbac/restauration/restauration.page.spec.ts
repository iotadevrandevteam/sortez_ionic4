import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurationPage } from './restauration.page';

describe('RestaurationPage', () => {
  let component: RestaurationPage;
  let fixture: ComponentFixture<RestaurationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
