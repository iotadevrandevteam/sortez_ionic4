import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParabonPage } from './parabon.page';

describe('ParabonPage', () => {
  let component: ParabonPage;
  let fixture: ComponentFixture<ParabonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParabonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParabonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
