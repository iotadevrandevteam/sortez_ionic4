import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifbonplanPage } from './modifbonplan.page';

describe('ModifbonplanPage', () => {
  let component: ModifbonplanPage;
  let fixture: ComponentFixture<ModifbonplanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifbonplanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifbonplanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
