import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParactPage } from './paract.page';

describe('ParactPage', () => {
  let component: ParactPage;
  let fixture: ComponentFixture<ParactPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParactPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParactPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
