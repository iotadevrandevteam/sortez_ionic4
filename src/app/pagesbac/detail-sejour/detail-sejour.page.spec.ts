import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSejourPage } from './detail-sejour.page';

describe('DetailSejourPage', () => {
  let component: DetailSejourPage;
  let fixture: ComponentFixture<DetailSejourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSejourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSejourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
