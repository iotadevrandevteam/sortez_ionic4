import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesGestionplatdujoursPage } from './pages-gestionplatdujours.page';

describe('PagesGestionplatdujoursPage', () => {
  let component: PagesGestionplatdujoursPage;
  let fixture: ComponentFixture<PagesGestionplatdujoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesGestionplatdujoursPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesGestionplatdujoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
