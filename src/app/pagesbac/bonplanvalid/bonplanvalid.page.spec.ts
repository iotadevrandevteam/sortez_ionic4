import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonplanvalidPage } from './bonplanvalid.page';

describe('BonplanvalidPage', () => {
  let component: BonplanvalidPage;
  let fixture: ComponentFixture<BonplanvalidPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonplanvalidPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonplanvalidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
