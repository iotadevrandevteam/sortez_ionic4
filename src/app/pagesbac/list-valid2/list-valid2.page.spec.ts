import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListValid2Page } from './list-valid2.page';

describe('ListValid2Page', () => {
  let component: ListValid2Page;
  let fixture: ComponentFixture<ListValid2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListValid2Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListValid2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
