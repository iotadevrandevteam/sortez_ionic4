import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListValidPage } from './list-valid.page';

describe('ListValidPage', () => {
  let component: ListValidPage;
  let fixture: ComponentFixture<ListValidPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListValidPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListValidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
