import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoconsoPage } from './infoconso.page';

describe('InfoconsoPage', () => {
  let component: InfoconsoPage;
  let fixture: ComponentFixture<InfoconsoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoconsoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoconsoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
