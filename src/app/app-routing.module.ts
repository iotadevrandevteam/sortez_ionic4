import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'addcapital', loadChildren: './pages/addcapital/addcapital.module#AddcapitalPageModule' },
  { path: 'addremise', loadChildren: './pages/addremise/addremise.module#AddremisePageModule' },
  { path: 'addtampon', loadChildren: './pages/addtampon/addtampon.module#AddtamponPageModule' },
  { path: 'bonplanlist', loadChildren: './pages/bonplanlist/bonplanlist.module#BonplanlistPageModule' },
  { path: 'bonplanvalid', loadChildren: './pages/bonplanvalid/bonplanvalid.module#BonplanvalidPageModule' },
  { path: 'client-list', loadChildren: './pages/client-list/client-list.module#ClientListPageModule' },
  { path: 'clientdetail', loadChildren: './pages/clientdetail/clientdetail.module#ClientdetailPageModule' },
  { path: 'detail-bonplan', loadChildren: './pages/detail-bonplan/detail-bonplan.module#DetailBonplanPageModule' },
  { path: 'detail-fidelity', loadChildren: './pages/detail-fidelity/detail-fidelity.module#DetailFidelityPageModule' },
  { path: 'detail-plat', loadChildren: './pages/detail-plat/detail-plat.module#DetailPlatPageModule' },
  { path: 'detail-sejour', loadChildren: './pages/detail-sejour/detail-sejour.module#DetailSejourPageModule' },
  { path: 'detail-table', loadChildren: './pages/detail-table/detail-table.module#DetailTablePageModule' },
  { path: 'forgottenpass', loadChildren: './pages/forgottenpass/forgottenpass.module#ForgottenpassPageModule' },
  // { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'infoconso', loadChildren: './pages/infoconso/infoconso.module#InfoconsoPageModule' },
  { path: 'list-detail', loadChildren: './pages/list-detail/list-detail.module#ListDetailPageModule' },
  { path: 'list-valid', loadChildren: './pages/list-valid/list-valid.module#ListValidPageModule' },
  { path: 'list-valid2', loadChildren: './pages/list-valid2/list-valid2.module#ListValid2PageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'mailclient', loadChildren: './pages/mailclient/mailclient.module#MailclientPageModule' },
  { path: 'modifbonplan', loadChildren: './pages/modifbonplan/modifbonplan.module#ModifbonplanPageModule' },
  { path: 'mon-compte', loadChildren: './pages/mon-compte/mon-compte.module#MonComptePageModule' },
  { path: 'pages-ajoutplat', loadChildren: './pages/pages-ajoutplat/pages-ajoutplat.module#PagesAjoutplatPageModule' },
  { path: 'pages-gestionplatdujours', loadChildren: './pages/pages-gestionplatdujours/pages-gestionplatdujours.module#PagesGestionplatdujoursPageModule' },
  { path: 'pages-updateplat', loadChildren: './pages/pages-updateplat/pages-updateplat.module#PagesUpdateplatPageModule' },
  { path: 'parabon', loadChildren: './pages/parabon/parabon.module#ParabonPageModule' },
  { path: 'parabp', loadChildren: './pages/parabp/parabp.module#ParabpPageModule' },
  { path: 'paracc', loadChildren: './pages/paracc/paracc.module#ParaccPageModule' },
  { path: 'paract', loadChildren: './pages/paract/paract.module#ParactPageModule' },
  { path: 'parametre', loadChildren: './pages/parametre/parametre.module#ParametrePageModule' },
  { path: 'parard', loadChildren: './pages/parard/parard.module#ParardPageModule' },
  { path: 'remise', loadChildren: './pages/remise/remise.module#RemisePageModule' },
  { path: 'reservation', loadChildren: './pages/reservation/reservation.module#ReservationPageModule' },
  { path: 'reservationhebergement', loadChildren: './pages/reservationhebergement/reservationhebergement.module#ReservationhebergementPageModule' },
  { path: 'reservationplat', loadChildren: './pages/reservationplat/reservationplat.module#ReservationplatPageModule' },
  { path: 'restaurant', loadChildren: './pages/restaurant/restaurant.module#RestaurantPageModule' },
  { path: 'restauration', loadChildren: './pages/restauration/restauration.module#RestaurationPageModule' },
  { path: 'sendsms', loadChildren: './pages/sendsms/sendsms.module#SendsmsPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
